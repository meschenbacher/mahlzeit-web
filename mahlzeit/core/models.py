from django.db import models
from django.contrib.auth import get_user_model


class Einkauf(models.Model):
    betrag = models.DecimalField(max_digits=8, decimal_places=2)
    kaeufer = models.ForeignKey(get_user_model(), on_delete=models.RESTRICT, related_name='kaeufer')
    datum = models.DateField()
    beschreibung = models.CharField(max_length=1024)
    kommentar = models.TextField(null=True, blank=True)
    esser = models.ManyToManyField(get_user_model(), related_name='esser')

    class Meta:
        verbose_name_plural = "Einkäufe"


class Bezahlung(models.Model):
    zahler = models.ForeignKey(get_user_model(), on_delete=models.RESTRICT, related_name='zahler')
    bezahlter = models.ForeignKey(get_user_model(), on_delete=models.RESTRICT, related_name='bezahlter')
    betrag = models.DecimalField(max_digits=8, decimal_places=2)
    datum = models.DateField()
    gebongt = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = "Bezahlungen"
