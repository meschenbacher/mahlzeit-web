from django.contrib import admin
from .models import *


@admin.register(Einkauf)
class EinkaufAdmin(admin.ModelAdmin):
    #  fields = [
        #  ('local_part', 'domain'),
        #  'target',
        #  'description',
        #  'expires',
        #  'expire_reason',
    #  ]
    #  list_display = [
        #  '__str__', 'description', 'created',
    #  ]
    #  search_fields = [
        #  'local_part',
        #  'domain__domain',
        #  'target__target',
        #  'description',
        #  'expire_reason',
    #  ]

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('kaeufer')
        return qs


@admin.register(Bezahlung)
class BezahlungAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('zahler', 'bezahlter')
        return qs
