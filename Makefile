PYTHON = ../venv/bin/python
COVERAGE = ../venv/bin/coverage
BASE = ./mahlzeit
SETTINGS = config.settings_local

run: collectstatic
	cd $(BASE) && $(PYTHON) ./manage.py runserver --settings=$(SETTINGS)

startapp:
	cd $(BASE) && $(PYTHON) ./manage.py startapp $(APPNAME)

makemigrations:
	cd $(BASE) && $(PYTHON) ./manage.py makemigrations --settings=$(SETTINGS)

migrate:
	cd $(BASE) && $(PYTHON) ./manage.py migrate --settings=$(SETTINGS)

loaddata:
	cd $(BASE) && $(PYTHON) ./manage.py loaddata fixtures/*.json --settings=$(SETTINGS)

collectstatic:
	cd $(BASE) && $(PYTHON) ./manage.py collectstatic --no-input --settings=$(SETTINGS)

test: collectstatic
	cd $(BASE) && $(PYTHON) ./manage.py test $(APPNAME) --settings=$(SETTINGS)

fasttest: collectstatic
	cd $(BASE) && $(PYTHON) ./manage.py test $(APPNAME) --exclude-tag=slow --settings=$(SETTINGS)

travistest:
	cd $(BASE) && python ./manage.py test --settings=$(SETTINGS)

coverage: collectstatic
	cd $(BASE) && $(COVERAGE) run ./manage.py test $(APPNAME) --settings=$(SETTINGS)

report:
	cd $(BASE) && $(COVERAGE) report

html:
	cd $(BASE) && $(COVERAGE) html
